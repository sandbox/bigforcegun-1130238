<?php
/**
 * @file
 * Administrative page callbacks for menu template module.
 */

/**
 * Menu callback which shows templates for selected menus with good edit form
 */

function menu_template_edit_ui_form($form, &$form_state, $menu) {
    global $menu_admin;
    $menu_name=$form_state['build_info']['args'][0]['menu_name'];
    $form['#attached']['css'] = array(drupal_get_path('module', 'menu') . '/menu.css');
    $sql = "
        SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.delivery_callback, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
        FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
        WHERE ml.menu_name = :menu
        ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";

    $result = db_query($sql, array(':menu' => $menu['menu_name']), array('fetch' => PDO::FETCH_ASSOC));

    $links = array();

    foreach ($result as $item) {
        $links[] = $item;
    }
    $tree = menu_tree_data($links);

    $node_links = array();
    menu_tree_collect_node_links($tree, $node_links);
    // We indicate that a menu administrator is running the menu access check.
    $menu_admin = TRUE;
    menu_tree_check_access($tree, $node_links);
    $menu_admin = FALSE;



    $form = array_merge($form, menu_template_ui_templates_table());

    $template_json = db_query('SELECT templates FROM {menu_custom} WHERE menu_name = :menu', array(':menu' => $form_state['build_info']['args'][0]['menu_name']))->fetchField();
    $template=json_decode($template_json, TRUE);


    if (!isset($template)) {
        $template=array();
    }
    $form['description_checkbox'] = array(
                '#type' => 'checkbox',
                '#title' => t('Description to Title'),
                '#name' => 'description_checkbox',
                '#default_value' => 0 ,

            );

    $form['links_table'] = menu_template_overview_tree_form($tree, $template);
    $form['#theme'] = 'menu_template_iu_overview_form';

    $form['#menu'] =  $menu;

    if (element_children($form)) {
        //pass
    }
    else {
        $form['#empty_text'] = t('There are no menu links yet. <a href="@link">Add link</a>.', array('@link' => url('admin/structure/menu/manage/' . $form['#menu']['menu_name'] . '/add')));
    }

    $form['actions'] = array('#type' => 'actions');

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );

    return $form;

  }



/**
 * Recursive helper function for menu_template_edit_ui_form().
 *
 * @param $tree
 *   The menu_tree retrieved by menu_tree_data.
 */

function menu_template_overview_tree_form($tree, $template=array()) {
    //menu_template_ui_set_variables();
    $form = &drupal_static(__FUNCTION__, array('#tree' => TRUE));
    static $root_menu=TRUE;
    static $template_states;
    if (empty($template_states)) {
        //include_once("menu_template_ui.install");
        //menu_template_ui_set_variables();
        $template_states=variable_get('menu_template_states');
    }

    foreach ($tree as $data) {
        $title = '';
        $item = $data['link'];
        // Don't show callbacks; these have $item['hidden'] < 0.
        if ($item && $item['hidden'] >= 0) {

            $mlid = 'mlid_' . $item['mlid'];

            $form[$mlid]['#item'] = $item;
            $form[$mlid]['#attributes'] = $item['hidden'] ? array('class' => array('menu-disabled')) : array('class' => array('menu-enabled'));
            $form[$mlid]['title']['#markup'] = l($item['title'], $item['href'], $item['localized_options']) . ($item['hidden'] ? ' (' . t('disabled') . ')' : '');


            $form[$mlid]['mlid'] = array(
                '#type' => 'hidden',
                '#value' => $item['mlid'],
            );

            $form[$mlid]['plid'] = array(
                '#type' => 'hidden',
                '#default_value' => $item['plid'],
            );

            $form[$mlid]['parent'] = array(
                    '#type' => 'hidden',
                    '#value' => FALSE,
                );

            if ($root_menu) {
                $form[$mlid]['root_menu'] = array(
                    '#type' => 'hidden',
                    '#value' => TRUE,
                );
                $form[$mlid]['templates'][$item['plid']]=array();
                $form[$mlid]['templates'][$item['plid']]=array_merge($form[$mlid]['templates'][$item['plid']], menu_template_parent_form($item, $template_states, $template, $item['plid']));
                $root_menu = FALSE;
            }

            //If there is a child - it means - the parent, then you can set a template
            if ($data['below']) {
                if (!isset($form[$mlid]['templates'][$item['mlid']])) {
                    $form[$mlid]['templates'][$item['mlid']]=array();
                }
                $form[$mlid]['parent']['#value']=TRUE;
                $form[$mlid]['templates'][$item['mlid']]=array_merge($form[$mlid]['templates'][$item['mlid']], menu_template_parent_form($item, $template_states, $template, $item['mlid']));
            }

        }

        if ($data['below']) {
            menu_template_overview_tree_form($data['below'], $template);
        }
    }
    return $form;
}

/**
 * Helper function for menu_template_overview_tree_form().
 * Generates a form for editing templates, based on the $template_states
 * @param
 *  - $item - why???
 *  - $template_states array with all states of menu link(for templates)
 *  - $template - array with saved templates
 *  - $lid - id of link(menu item)
 * @return $form
 *      array of form elements
 */



function menu_template_parent_form($item, $template_states, $template, $lid) {

    $form = array(
        '#title' => '$template[' . $lid . ']',
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    //Running on the type of item
    foreach ($template_states as $type => $type_array) {
        //Create a checkbox for the current type

        $form[$type]['checkbox'] = array(
                '#type' => 'checkbox',
                '#title' => t($type_array['title']),
                '#name' => 'template_on[' . $lid . '][' . $type . ']',
                '#default_value' => 0 ,
                '#description' => $type_array['description'],
            );
        //Save the javascript for the next container, attached to checkbox
        $hide_states = array(
                    'visible' => array(
                        'input[name="template_on[' . $lid . '][' . $type . ']"]' => array('checked' => TRUE),
                    ),
                );

        $template_value='';

        //If you have an array $template[$lid][$type] => there is at least one template => checkbox pressed
        if (isset($template[$lid][$type])) {
                $template_value=$template[$lid][$type];
                $form[$type]['checkbox']['#default_value']= 1;
        }
        //TODO: Finish the algorithm (create a universal, for every $template_states)

        $form[$type]['textfields']=array(
            '#type' => 'container',
            '#states' => $hide_states,
            );




        if (!isset($type_array['states'])) {

            $title='$template[' . $lid . '][\'' . $type . '\']';
            $name='template[' . $lid . '][' . $type . ']';

            $form[$type]['textfields']['qw'] = array(
                '#type' => 'textfield',
                '#default_value' => $template_value,
                '#size' => 100,
                '#title' => $title,
                '#name' => $name,
                '#description' => $type_array['description'],
            );
        }
        else{
            $header=array();
            $rows=array();
            foreach ($type_array['states'] as $active_trail_state => $active_trail_state_array) {

                if (!in_array($active_trail_state_array['description'], $header)) {
                    $header[]=$active_trail_state_array['description'];
                }
                foreach ($active_trail_state_array['states'] as $active_state => $active_state_array) {

                    $title='$template[' . $lid . '][\'' . $type . '\'][\'' . $active_trail_state . '\'][\'' . $active_state . '\']';
                    $name='template[' . $lid . '][' . $type . '][' . $active_trail_state . '][' . $active_state . ']';
                    if (isset($template[$lid][$type][$active_trail_state][$active_state])) {
                        $template_value=$template[$lid][$type][$active_trail_state][$active_state];
                    }
                    else{
                        $template_value='';
                    }

                    $temp = array(
                        '#type' => 'textfield',
                        '#value' => $template_value,
                        //'#size' => 100,
                        '#title' => $title,
                        '#name' => $name,
                        '#description' => $active_state_array['description'],
                    );

                    $row[$active_state][]=drupal_render($temp);
                }
            }

        foreach ($row as $row_value) {

            $rows[]=$row_value;
        }

        $row=array();

        $form[$type]['textfields'][$active_trail_state]['table'] = array(
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $rows,
            '#empty' => t('0_o'),
            );



        }
    }



    return $form;
}


/**
 * Submit handler for the template edit ui form.
 *
 * This function treat the array and stores it in database
 *
 *
 * @see menu_template_edit_ui_form()
 */

function menu_template_edit_ui_form_submit($form, &$form_state) {

    $template=$form_state['input']['template'];
    //Clean the incoming array
    //OMG!!!!
    foreach ($template as $lid => $types) {
        foreach ($types as $type => $active_trail_states) {
            if (is_array($active_trail_states)) {
                foreach ($active_trail_states as $active_trail_state => $active_states) {
                    foreach ($active_states as $active_state => $active_state_value) {
                        if (empty($active_state_value)) {
                            unset($template[$lid][$type][$active_trail_state][$active_state]);
                        }
                    }
                    if (empty($template[$lid][$type][$active_trail_state]))
                        unset($template[$lid][$type][$active_trail_state]);
                }
            }
            if (empty($template[$lid][$type]))
                unset($template[$lid][$type]);
        }
        if (empty($template[$lid]))
            unset($template[$lid]);
    }


    $menu_name=$form_state['build_info']['args'][0]['menu_name'];
    $template_json=json_encode($template);
    db_update('menu_custom')->fields(array('templates' => $template_json))->condition('menu_name', $menu_name )->execute();
    drupal_set_message(t('Settings saved'), $type = 'status');
}

/**
 * Helper function for menu_template_edit_master_form().
 * Convert array to string of php code
 * @param $array
 *      array, which must be converted
 *
 * @return $output
 *      string with php code of array
 */

function menu_template_get_php_string($template) {
    $output='';
    if (!empty($template)) {
        foreach ($template as $lid => $types) {
            foreach ($types as $type => $active_trail_states) {
                if (!is_array($active_trail_states)) {
                    if (!empty($active_trail_states)) {//0_0
                        $template=$active_trail_states;
                        $output .= '$template[' . $lid . '][\'' . $type . '\'] = \'' . $template . '\';';
                        $output .= "\n";
                    }
                }

                if (is_array($active_trail_states)) {
                    foreach ($active_trail_states as $active_trail_state => $active_states) {
                        foreach ($active_states as $active_state => $template) {
                            if (!empty($template)) {
                                $output .= '$template[' . $lid . '][\'' . $type . '\'][\'' . $active_trail_state . '\'][\'' . $active_state . '\'] = \'' . $template . '\';';
                                $output .= "\n";
                            }
                        }
                    }
                }
            }
        }
    }
    return $output;
}

/**
 * Menu callback which shows templates for selected menus with master edit form
 */

function menu_template_edit_master_form($form, &$form_state) {

    $template_json= db_query('SELECT templates FROM {menu_custom} WHERE menu_name = :menu', array(':menu' => $form_state['build_info']['args'][0]['menu_name']))->fetchField();//->fetchAll();

    $form = array_merge($form, menu_template_ui_templates_table());

    $form['template_string'] = array(
        '#title' => t('PHP code'),
        '#type' => 'textarea',
        '#cols' => 60,
        '#rows' => 15,
        '#default_value' => menu_template_get_php_string(json_decode($template_json, TRUE)),
        '#required' => FALSE,
        '#description' => htmlspecialchars(t('Carefully use the PHP syntax.No forget to escape quotes.Example : $template[6][\'prefix\'] = "<li %drupal_classes id=\"menu_link_%mlid\">%drupal_link %sub_menu</li>"')),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save configuration'),
    );

    return $form;
}


/**
 * Helper function for menu_template_edit_master_form().
 * Generates a table with all tokens and descriplions

 * @return $form
 *      array with table
 */

function menu_template_ui_templates_table() {
    $header = array(
        array('data' => t('Name') , 'sort' => 'desc'),
        array('data' => t('Token')),
        array('data' => t('Description')),
    );


    $rows = variable_get('menu_template_rows');

     $form['template_variables'] = array(
        '#title' => t('Replacement patterns'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

    $form['template_variables']['table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('0_o'),
    );

    return $form;
}

/**
 * Submit handler for the template edit master form.
 *
 * This function treat the array and stores it in database
 *
 *
 * @see menu_template_edit_master_form()
 */


function menu_template_edit_master_form_submit($form, &$form_state) {

    $template_string=$form_state['input']['template_string'];

    if (@eval('return true;' . $template_string)) {
        //If no errors
        $menu_name=$form_state['build_info']['args'][0]['menu_name'];
        eval($template_string);
        if (!empty($template_string)) {
            if (!isset($template)) {
                drupal_set_message(t('Array $template does not exist.'), 'error', FALSE);
            }
            else{
                if (!is_array($template)) {
                    drupal_set_message(t('$template is not an array'), 'error');
                }
                else{
                    $template_json=json_encode($template);
                    db_update('menu_custom')->fields(array('templates' => $template_json))->condition('menu_name', $menu_name )->execute();
                    drupal_set_message(t('Settings saved'), $type = 'status');
                }
            }
        }
        else{
            db_update('menu_custom')->fields(array('templates' => ''))->condition('menu_name', $menu_name )->execute();
            drupal_set_message(t('Settings saved'), $type = 'status');
            drupal_set_message(t('Templates cleaned'), $type = 'status');
        }
    }
    else{
        drupal_set_message(t('Error in php syntax'), $type = 'error');
    }

}
?>
