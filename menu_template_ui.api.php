<?php

/*
 *
 * name: new_menu_tree
 * Slightly altered function of menu.inc( menu_tree() ), which simply allows you to pass a more options
 *
 * @param
 * - $menu_name -
 * - $plid - Id of menu item from which you want to start rendering the menu
 * - $max_depth
 * - $only_active_trail - render only active trail (only active tree of menu)
 * @return
 *  $menu_output - array for render with menu
 */


function new_menu_tree($menu_name,$plid=0,$max_depth = NULL,$only_active_trail = FALSE) {
  $menu_output = &drupal_static(__FUNCTION__, array());

  if (!isset($menu_output[$menu_name])) {
    $tree = new_menu_tree_page_data($menu_name,$plid,$max_depth,$only_active_trail);
    $menu_output[$menu_name] = menu_tree_output($tree);
  }
  return $menu_output[$menu_name];
}


/*
 *
 * name: new_menu_new_menu_tree_page_datatree
 * Slightly altered function of menu.inc( menu_tree_page_data() ),
 *   Which takes one parameter more ($plid), and rearranges the output data so that the following functions of menu.inc collected menus from $plid
 *
 * @param
 * - $menu_name -
 * - $plid - Id of menu item from which you want to start rendering the menu
 * - $max_depth
 * - $only_active_trail - render only active trail (only active tree of menu)
 * @return
 *  $tree for new_menu_tree();
 */


function new_menu_tree_page_data($menu_name,$plid=0,$max_depth = NULL, $only_active_trail = FALSE) {
  $tree = &drupal_static(__FUNCTION__, array());
  // Load the menu item corresponding to the current page.
  if ($item = menu_get_item()) {
    if (isset($max_depth)) {
      $max_depth = min($max_depth, MENU_MAX_DEPTH);
    }
    // Generate a cache ID (cid) specific for this page.
    $cid = 'links:' . $menu_name . ':page:' . $item['href'] . ':' . $GLOBALS['language']->language . ':' . (int) $item['access'] . ':' . (int) $max_depth;
    // If we are asked for the active trail only, and $menu_name has not been
    // built and cached for this page yet, then this likely means that it
    // won't be built anymore, as this function is invoked from
    // template_process_page(). So in order to not build a giant menu tree
    // that needs to be checked for access on all levels, we simply check
    // whether we have the menu already in cache, or otherwise, build a minimum
    // tree containing the breadcrumb/active trail only.
    // @see menu_set_active_trail()
    if (!isset($tree[$cid]) && $only_active_trail) {
      $cid .= ':trail';
    }
    //>>modify original fucntion
    $plid_link=menu_link_load($plid);
    if(!empty($plid_link)){
        $plid_depth=$plid_link['depth']+1;
        //If the output of the menu through api function(THIS function), to the cache will be asking for it, but not for general
        $cid.=":plid:".$plid;
    }
    //for root
    else{
        $plid_depth=1;
    }
    //>>end of modify original fucntion
    if (!isset($tree[$cid])) {
      // If the static variable doesn't have the data, check {cache_menu}.
      $cache = cache_get($cid, 'cache_menu');
      if ($cache && isset($cache->data)) {
        // If the cache entry exists, it contains the parameters for
        // menu_build_tree().
        $tree_parameters = $cache->data;
      }
      // If the tree data was not in the cache, build $tree_parameters.
      if (!isset($tree_parameters)) {

        //>>modify original fucntion
        $tree_parameters = array(
          'min_depth' => $plid_depth,
          'max_depth' => $max_depth,
        );
        // Parent mlids; used both as key and value to ensure uniqueness.
        // We always want all the top-level links with plid == 0.
        $active_trail = array($plid => $plid);
        //>>end of modify original fucntion
        // If the item for the current page is accessible, build the tree
        // parameters accordingly.

        if ($item['access']) {
          // Find a menu link corresponding to the current path.
          if ($active_link = menu_link_get_preferred()) {
            // The active link may only be taken into account to build the
            // active trail, if it resides in the requested menu. Otherwise,
            // we'd needlessly re-run _menu_build_tree() queries for every menu
            // on every page.

            if ($active_link['menu_name'] == $menu_name) {

              // Use all the coordinates, except the last one because there
              // can be no child beyond the last column.
              for ($i = 1; $i < MENU_MAX_DEPTH; $i++) {
                //>>modify original fucntion
                if ($active_link['p' . $i]==$plid) {
                //>>end of modify original fucntion

                //if ($active_link['p' . $i]) {
                  $active_trail[$active_link['p' . $i]] = $active_link['p' . $i];
                }
              }

              // If we are asked to build links for the active trail only, skip
              // the entire 'expanded' handling.
              if ($only_active_trail) {
                $tree_parameters['only_active_trail'] = TRUE;
              }
            }
          }
          $parents = $active_trail;

          $expanded = variable_get('menu_expanded', array());
          // Check whether the current menu has any links set to be expanded.
          if (!$only_active_trail && in_array($menu_name, $expanded)) {
            // Collect all the links set to be expanded, and then add all of
            // their children to the list as well.
            do {
              $result = db_select('menu_links', NULL, array('fetch' => PDO::FETCH_ASSOC))
                ->fields('menu_links', array('mlid'))
                ->condition('menu_name', $menu_name)
                ->condition('expanded', 1)
                ->condition('has_children', 1)
                ->condition('plid', $parents, 'IN')
                ->condition('mlid', $parents, 'NOT IN')
                ->execute();
              $num_rows = FALSE;

              foreach ($result as $item) {
                $parents[$item['mlid']] = $item['mlid'];
                $num_rows = TRUE;
              }
            } while ($num_rows);

          }
          $tree_parameters['expanded'] = $parents;
          $tree_parameters['active_trail'] = $active_trail;
        }
        // If access is denied, we only show top-level links in menus.
        else {
          $tree_parameters['expanded'] = $active_trail;
          $tree_parameters['active_trail'] = $active_trail;
        }
        // Cache the tree building parameters using the page-specific cid.
        cache_set($cid, $tree_parameters, 'cache_menu');
      }

      // Build the tree using the parameters; the resulting tree will be cached
      // by _menu_build_tree().
      $tree[$cid] = menu_build_tree($menu_name, $tree_parameters);
    }
    return $tree[$cid];
  }
  return array();
}
?>
