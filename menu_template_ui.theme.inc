<?php

/**
 * @file
 * Perform bulk exports.
 */

/**
 * Returns HTML for a form elemetns and table of templates for the menu templates overview page.
 * name: theme_menu_template_iu_overview_form
 * @param $variables
 *   An associative array containing:
 *   - Conventional elements of the form
 *   - links_table : An associative array containing elements fow spesial table
 * @return $output
 * @ingroup themeable
 */

function theme_menu_template_iu_overview_form($variables) {
    $form = $variables['form'];

    $header = array(
        t('Menu link'),
    );

    $rows = array();
    $output = '';
    foreach (element_children($form) as $id) {

        if ($id=='links_table') {
            foreach (element_children($form[$id]) as $mlid) {
                $element = &$form[$id][$mlid];

                // Change the parent field to a hidden. This allows any value but hides the field.
                $element['plid']['#type'] = 'hidden';
                $row = array();
                $temp=array();

                if (isset($element['root_menu'])) {
                    $row[] = theme('indentation_visible', array('size' => $element['#item']['depth'] - 1)) . drupal_render($element['templates'][$element['plid']['#value']]);

                    $row = array_merge(array('data' => $row), $element['#attributes']);

                    $rows[] = $row;
                    $row = array();
                }

                $row[] = theme('indentation_visible', array('size' => $element['#item']['depth'] - 1)) . drupal_render($element['title']) . drupal_render($element['templates'][$element['mlid']['#value']]);
                $row = array_merge(array('data' => $row), $element['#attributes']);
                $rows[] = $row;
            }

            if (empty($rows)) {
                $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '7'));
            }
            $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'menu-overview')));
        }
        else{
            $output .= drupal_render($form[$id]);
        }


    }

    return $output;
}

/**
 * Returns HTML for visible indentation (aka theme_indentation) for the menu templates overview page.
 * name: theme_indentation_visible
 * @param $variables
 *   An associative array containing:
 *   - size - size of indentations
 * @return $output
 * @ingroup themeable
 */



function theme_indentation_visible($variables) {
  $output = '';
  for ($n = 0; $n < $variables['size']; $n++) {
    if ($n==0)
        $output .= '<div class="indentation tree-child">&nbsp;</div>';
    else
        $output .= '<div class="indentation tree-child-horizontal">&nbsp;</div>';
  }
  return $output;
}
?>
